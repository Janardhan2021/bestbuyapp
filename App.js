import * as React from 'react';

import Drawer from './src/modules/utils/Drawer';
import Navigation from './src/modules/utils/Navigation';

function App() {
  return <Navigation />;
}

export default App;
